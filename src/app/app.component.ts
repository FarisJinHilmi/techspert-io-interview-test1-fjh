import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { AppService } from './app.service';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'techspert-io-interview-test1-fjh';
  paginatorDefault = '10';
  displayedColumns = ['name', 'id', 'temp', 'humidity', 'speed', 'weather'];
  dataSource: any;
  selectedCity: string;
  allCities: string[];

  public barChartOptions: ChartOptions = {
    responsive: true,
  };
  public barChartType: ChartType = 'bar';
  public barChartLegend = false;
  public barChartPlugins = [];
  public barChartLabels: Label[] = [];
  public barChartData: ChartDataSets[] = [
    { data: [], label: '' },
  ];

  meanTemperature: string;
  allTableData: any;

  isUnitTest: boolean = false;

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(
    public appService: AppService,
  ) {}

  ngOnInit(): void {
    if (!this.isUnitTest) {
      this.appService.getLocationsByIds(
        // tslint:disable-next-line: max-line-length
        [2643743, 4920327, 707860, 1269752, 6541787, 596826, 2974652, 2834136, 1973733, 935111, 5164057, 5816882, 590497, 2272185, 727762, 1730789, 2858548, 2817659, 3033367, 5106841, 6454406, 250052, 4294349]
      ).subscribe((data: any) => {
        if (data) {
          this.formatLocations(data.list);
        }
      });
    }
  }

  formatLocations(locations: any[]) {
    this.allCities = ['All'];
    this.selectedCity = this.allCities[0];
    const tableData = [];
    this.barChartLabels = [];
    const temperatureList = [];
    let cumulativeTemp = 0;
    locations.forEach((location: any, index: number) => {
      this.barChartLabels.push(location.name);
      this.allCities.push(location.name);
      temperatureList.push(location.main.temp);
      tableData.push({
        name: location.name,
        id: location.id,
        temp: location.main.temp,
        humidity: location.main.humidity,
        speed: location.wind.speed,
        weather: location.weather[0].description,
      });
      cumulativeTemp += +location.main.temp;
      if (index === locations.length - 1) {
        this.allTableData = tableData;
        this.barChartData = [{ data: temperatureList, label: 'Temperature (Celcius)' }];
        this.meanTemperature = (cumulativeTemp / locations.length).toFixed(2);
        this.dataSource = new MatTableDataSource<any>(tableData);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }

  onFilterChange(city: string) {
    if (city === 'All') {
      this.dataSource = new MatTableDataSource<any>(this.allTableData);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    } else {
      const tableData = this.allTableData.filter(location => location.name === city);
      this.dataSource = new MatTableDataSource<any>(tableData);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
  }
}
