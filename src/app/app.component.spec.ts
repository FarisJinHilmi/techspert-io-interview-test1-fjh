import { TestBed, async, fakeAsync } from '@angular/core/testing';
import { AppComponent } from './app.component';

import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { MatTableModule, MatPaginatorModule, MatSortModule, MatOptionModule, MatSelectModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ChartsModule } from 'ng2-charts';
import { FormsModule } from '@angular/forms';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { AppService } from './app.service';

describe('AppComponent', () => {

  const testData = [
    {coord: {lon: -0.13, lat: 51.51}, sys: {country: 'GB', timezone: 0, sunrise: 1578297909, sunset: 1578326797}, weather: [{id: 801, main: 'Clouds', description: 'few clouds', icon: '02n'}], main: {temp: 7.13, feels_like: 3.01, temp_min: 6, temp_max: 8.33, pressure: 1020, humidity: 93}, visibility: 10000, wind: {speed: 4.6, deg: 260}, clouds: {all: 22}, dt: 1578344959, id: 2643743, name: 'London'},
    {coord: {lon: 30.52, lat: 50.43}, sys: {country: 'UA', timezone: 7200, sunrise: 1578290252, sunset: 1578319737}, weather: [{id: 803, main: 'Clouds', description: 'broken clouds', icon: '04n'}], main: {temp: -2.42, feels_like: -5.79, temp_min: -4, temp_max: -1.67, pressure: 1028, humidity: 79}, visibility: 9000, wind: {speed: 1, deg: 260}, clouds: {all: 75}, dt: 1578344947, id: 703448, name: 'Kiev'},
    {coord: {lon: 37.62, lat: 55.75}, sys: {country: 'RU', timezone: 10800, sunrise: 1578376608, sunset: 1578402824}, weather: [{id: 803, main: 'Clouds', description: 'broken clouds', icon: '04n'}], main: {temp: -1.8, feels_like: -8.1, temp_min: -2, temp_max: -1, pressure: 1023, humidity: 68}, visibility: 10000, wind: {speed: 5, deg: 210}, clouds: {all: 75}, dt: 1578344940, id: 524901, name: 'Moscow'},
    {coord: {lon: -86.55, lat: 41.57}, sys: {country: 'US', timezone: -21600, sunrise: 1578316372, sunset: 1578349828}, weather: [{id: 800, main: 'Clear', description: 'clear sky', icon: '01d'}], main: {temp: 5.02, feels_like: -1.54, temp_min: 3.89, temp_max: 6.11, pressure: 1020, humidity: 35}, visibility: 16093, wind: {speed: 5.1, deg: 240}, clouds: {all: 1}, dt: 1578345276, id: 4920327, name: 'Fish Lake'},
    {coord: {lon: 34.28, lat: 44.55}, sys: {country: 'UA', timezone: 10800, sunrise: 1578374349, sunset: 1578406687}, weather: [{id: 804, main: 'Clouds', description: 'overcast clouds', icon: '04n'}], main: {temp: 6.68, feels_like: -0.34, temp_min: 6.68, temp_max: 6.68, pressure: 1019, sea_level: 1019, grnd_level: 1018, humidity: 74}, wind: {speed: 7.73, deg: 34}, clouds: {all: 100}, dt: 1578344723, id: 707860, name: 'Hurzuf'},
    {coord: {lon: 79.67, lat: 26.94}, sys: {country: 'IN', timezone: 19800, sunrise: 1578360686, sunset: 1578398557}, weather: [{id: 804, main: 'Clouds', description: 'overcast clouds', icon: '04n'}], main: {temp: 16.02, feels_like: 13.28, temp_min: 16.02, temp_max: 16.02, pressure: 1017, sea_level: 1017, grnd_level: 1000, humidity: 48}, wind: {speed: 2.31, deg: 88}, clouds: {all: 100}, dt: 1578344952, id: 1269752, name: 'Indergarh'},
    {coord: {lon: 11.12, lat: 43.95}, sys: {country: 'IT', timezone: 3600, sunrise: 1578293393, sunset: 1578325911}, weather: [{id: 800, main: 'Clear', description: 'clear sky', icon: '01n'}], main: {temp: 1.86, feels_like: -1.6, temp_min: -1.11, temp_max: 4.44, pressure: 1028, humidity: 69}, visibility: 10000, wind: {speed: 1.5, deg: 110}, clouds: {all: 0}, dt: 1578345196, id: 6541787, name: 'Vaiano'},
    {coord: {lon: -1.06, lat: 43.68}, sys: {country: 'FR', timezone: 3600, sunrise: 1578296262, sunset: 1578328891}, weather: [{id: 804, main: 'Clouds', description: 'overcast clouds', icon: '04n'}], main: {temp: 9.04, feels_like: 8.13, temp_min: 4, temp_max: 12, pressure: 1032, humidity: 100}, visibility: 1800, wind: {speed: 1, deg: 140}, clouds: {all: 100}, dt: 1578345150, id: 2974652, name: 'Seyresse'},
    {coord: {lon: 7.18, lat: 49.7}, sys: {country: 'DE', timezone: 3600, sunrise: 1578295658, sunset: 1578325538}, weather: [{id: 701, main: 'Mist', description: 'mist', icon: '50n'}], main: {temp: 0.1, feels_like: -4.51, temp_min: -1.11, temp_max: 1.67, pressure: 1023, humidity: 94}, visibility: 1000, wind: {speed: 3.6, deg: 210}, clouds: {all: 90}, dt: 1578345107, id: 2834136, name: 'Schwollen'},
    {coord: {lon: 120.3, lat: -3.23}, sys: {country: 'ID', timezone: 28800, sunrise: 1578347730, sunset: 1578392004}, weather: [{id: 804, main: 'Clouds', description: 'overcast clouds', icon: '04n'}], main: {temp: 27.13, feels_like: 31.41, temp_min: 27.13, temp_max: 27.13, pressure: 1009, sea_level: 1009, grnd_level: 1007, humidity: 81}, wind: {speed: 1.84, deg: 350}, clouds: {all: 100}, dt: 1578344747, id: 1973733, name: 'Padangsappa'},
    {coord: {lon: 31.13, lat: -25.97}, sys: {country: 'SZ', timezone: 7200, sunrise: 1578280200, sunset: 1578329497}, weather: [{id: 803, main: 'Clouds', description: 'broken clouds', icon: '04n'}], main: {temp: 21.44, feels_like: 21.04, temp_min: 18, temp_max: 23.89, pressure: 1020, humidity: 77}, wind: {speed: 4.1, deg: 50}, clouds: {all: 76}, dt: 1578344728, id: 935111, name: 'Bulembu'},
    {coord: {lon: -84.13, lat: 41.39}, sys: {country: 'US', timezone: -18000, sunrise: 1578315758, sunset: 1578349280}, weather: [{id: 800, main: 'Clear', description: 'clear sky', icon: '01d'}], main: {temp: 5.22, feels_like: -0.25, temp_min: 3.33, temp_max: 6.11, pressure: 1019, humidity: 48}, visibility: 16093, wind: {speed: 4.1, deg: 240}, clouds: {all: 1}, dt: 1578345285, id: 5164057, name: 'Napoleon'},
    {coord: {lon: -106.44, lat: 42.88}, sys: {country: 'US', timezone: -25200, sunrise: 1578321393, sunset: 1578354357}, weather: [{id: 800, main: 'Clear', description: 'clear sky', icon: '01d'}], main: {temp: 1.82, feels_like: -9.1, temp_min: 1, temp_max: 2.22, pressure: 1021, humidity: 43}, visibility: 14484, wind: {speed: 11.3, deg: 270}, clouds: {all: 1}, dt: 1578345298, id: 5816882, name: 'Air Base Acres'},
    {coord: {lon: 27.03, lat: 59.38}, sys: {country: 'EE', timezone: 7200, sunrise: 1578294354, sunset: 1578317312}, weather: [{id: 804, main: 'Clouds', description: 'overcast clouds', icon: '04n'}], main: {temp: 3.13, feels_like: -1.36, temp_min: 3.13, temp_max: 3.13, pressure: 1016, sea_level: 1016, grnd_level: 1014, humidity: 92}, wind: {speed: 4.02, deg: 220}, clouds: {all: 100}, dt: 1578344717, id: 590497, name: 'Lüganuse'},
    {coord: {lon: -8.35, lat: 37.13}, sys: {country: 'PT', timezone: 0, sunrise: 1578296841, sunset: 1578331812}, weather: [{id: 800, main: 'Clear', description: 'clear sky', icon: '01n'}], main: {temp: 10.97, feels_like: 8.9, temp_min: 10, temp_max: 12.78, pressure: 1030, humidity: 87}, visibility: 10000, wind: {speed: 2.6, deg: 340}, clouds: {all: 0}, dt: 1578345059, id: 2272185, name: 'Alcantarilha'},
    {coord: {lon: 36.03, lat: 49.76}, sys: {country: 'UA', timezone: 7200, sunrise: 1578288751, sunset: 1578318593}, weather: [{id: 803, main: 'Clouds', description: 'broken clouds', icon: '04n'}], main: {temp: -1, feels_like: -5.83, temp_min: -1, temp_max: -1, pressure: 1026, humidity: 68}, visibility: 10000, wind: {speed: 3, deg: 360}, clouds: {all: 75}, dt: 1578344727, id: 812881, name: 'Kislovskiy'},
    {coord: {lon: 25.28, lat: 41.4}, sys: {country: 'BG', timezone: 7200, sunrise: 1578289503, sunset: 1578323002}, weather: [{id: 804, main: 'Clouds', description: 'overcast clouds', icon: '04n'}], main: {temp: 4.44, feels_like: -1.85, temp_min: 4.44, temp_max: 4.44, pressure: 1018, humidity: 53}, wind: {speed: 5.36, deg: 16}, clouds: {all: 100}, dt: 1578344724, id: 727762, name: 'Rastnik'},
    {coord: {lon: 123.25, lat: 10.92}, sys: {country: 'PH', timezone: 28800, sunrise: 1578348438, sunset: 1578389880}, weather: [{id: 803, main: 'Clouds', description: 'broken clouds', icon: '04n'}], main: {temp: 26.46, feels_like: 28.25, temp_min: 26.46, temp_max: 26.46, pressure: 1011, sea_level: 1011, grnd_level: 1011, humidity: 80}, wind: {speed: 4.72, deg: 81}, clouds: {all: 61}, dt: 1578344739, id: 1730789, name: 'Andres Bonifacio'},
    {coord: {lon: 12.5, lat: 50.88}, sys: {country: 'DE', timezone: 3600, sunrise: 1578294700, sunset: 1578323942}, weather: [{id: 800, main: 'Clear', description: 'clear sky', icon: '01n'}], main: {temp: -0.51, feels_like: -5.36, temp_min: -2.78, temp_max: 1.67, pressure: 1022, humidity: 86}, visibility: 10000, wind: {speed: 3.6, deg: 180}, clouds: {all: 0}, dt: 1578345106, id: 2858548, name: 'Oberwiera'},
    {coord: {lon: 8.07, lat: 49.87}, sys: {country: 'DE', timezone: 3600, sunrise: 1578295489, sunset: 1578325279}, weather: [{id: 741, main: 'Fog', description: 'fog', icon: '50n'}], main: {temp: -0.11, feels_like: -4.44, temp_min: -1.67, temp_max: 1.67, pressure: 1025, humidity: 92}, visibility: 500, wind: {speed: 3.1, deg: 210}, clouds: {all: 90}, dt: 1578345106, id: 2817659, name: 'Vendersheim'}
  ];

  // let httpMock: HttpTestingController;
  // let service: AppService;

  // const createFakeFile = (fileName: string = 'fileName'): File => {
  //   const blob = new Blob([''], { type: 'text/html' });
  //   blob['lastModifiedDate'] = '';
  //   blob['name'] = fileName;
  //   return <File>blob;
  // };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
      ],
      imports: [
        BrowserModule,
        HttpClientModule,
        MatTableModule, MatPaginatorModule, MatSortModule, MatOptionModule, MatSelectModule,
        BrowserAnimationsModule,
        ChartsModule,
        FormsModule,
        HttpClientTestingModule,
      ],
      providers: [
        AppService,
      ]
    }).compileComponents();

    // httpMock = TestBed.get(HttpTestingController);
    // service = TestBed.get(AppService);
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should format initial data for the table ', (done) => {
    const fixture = TestBed.createComponent(AppComponent);
    const component = fixture.componentInstance;
    component.isUnitTest = true;
    setTimeout(() => {
      component.formatLocations(testData);
    }, 100);
    fixture.autoDetectChanges(true);
    setTimeout(() => {
      expect(component.allTableData).toBeTruthy();
      done();
    }, 3000);
  });

  it('should filter table data on filter change ', (done) => {
    const fixture = TestBed.createComponent(AppComponent);
    const component = fixture.componentInstance;
    component.isUnitTest = true;
    setTimeout(() => {
      component.formatLocations(testData);
    }, 100);
    fixture.autoDetectChanges(true);
    setTimeout(() => {
      component.selectedCity = component.allCities[1];
      component.onFilterChange(component.selectedCity);
      setTimeout(() => {
        expect(component.allTableData[0].name).toEqual(component.selectedCity);
        done();
      }, 1000);
    }, 3000);
  });

  it('should update HTML when table updated ', (done) => {
    const fixture = TestBed.createComponent(AppComponent);
    const component = fixture.componentInstance;
    component.isUnitTest = true;
    setTimeout(() => {
      component.formatLocations(testData);
    }, 100);
    fixture.autoDetectChanges(true);
    setTimeout(() => {
      component.selectedCity = component.allCities[1];
      component.onFilterChange(component.selectedCity);
      setTimeout(() => {
        const tableRows = fixture.nativeElement.querySelectorAll('mat-row');
        expect(tableRows[0].innerHTML.includes(component.selectedCity)).toBeTruthy();
        done();
      }, 1000);
    }, 3000);
  });

  it('should populate 5 rows of table data in html', (done) => {
    const fixture = TestBed.createComponent(AppComponent);
    const component = fixture.componentInstance;
    component.isUnitTest = true;
    component.paginatorDefault = '5';
    setTimeout(() => {
      component.formatLocations(testData);
    }, 100);
    fixture.autoDetectChanges(true);
    setTimeout(() => {
      const tableRows = fixture.nativeElement.querySelectorAll('mat-row');
      expect(tableRows.length).toEqual(5);
      done();
    }, 3000);
  });

  it('should populate 10 rows of table data in html', (done) => {
    const fixture = TestBed.createComponent(AppComponent);
    const component = fixture.componentInstance;
    component.isUnitTest = true;
    component.paginatorDefault = '10';
    setTimeout(() => {
      component.formatLocations(testData);
    }, 100);
    fixture.autoDetectChanges(true);
    setTimeout(() => {
      const tableRows = fixture.nativeElement.querySelectorAll('mat-row');
      expect(tableRows.length).toEqual(10);
      done();
    }, 3000);
  });

  it('should populate 20 rows of table data in html', (done) => {
    const fixture = TestBed.createComponent(AppComponent);
    const component = fixture.componentInstance;
    component.isUnitTest = true;
    component.paginatorDefault = '20';
    setTimeout(() => {
      component.formatLocations(testData);
    }, 100);
    fixture.autoDetectChanges(true);
    setTimeout(() => {
      const tableRows = fixture.nativeElement.querySelectorAll('mat-row');
      expect(tableRows.length).toEqual(20);
      done();
    }, 3000);
  });

});
