import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  private httpOptions = {
    headers: new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': '194333f5b09188fbda8c4a3bbfea30b2'
    })
  };

    // 'https://cors-anywhere.herokuapp.com/' being used as openweathermap api is rejecting preflight option due to no key
  private baseUrl = 'https://cors-anywhere.herokuapp.com/http://api.openweathermap.org/data/';
  private options = '&units=metric';

  constructor(
    protected httpClient: HttpClient,
  ) {}

  public getLocationsByIds(locationIds: number[]): Observable<any> {
      let url = this.baseUrl + '2.5/group?id=';
      for (const [index, id] of locationIds.entries()) {
        url += id.toString();
        if (index === locationIds.length - 1 || index === 19) { // request will fail if > 20 entries given
          url += this.options;
          break;
        } else {
          url += ',';
        }
      }

      // just for testing
      // url = this.baseUrl + '2.5/find?lat=55.5&lon=37.5&cnt=30' + this.options;
      return this.httpClient.get<any>(url, this.httpOptions);
  }
}
